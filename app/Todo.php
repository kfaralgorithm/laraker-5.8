<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use AustinHeap\Database\Encryption\Traits\HasEncryptedAttributes;
use Illuminate\Database\Eloquent\SoftDeletes;


class Todo extends Model
{
	use SoftDeletes, HasEncryptedAttributes;

	/** @var array The attributes that should be encrypted/decrypted */
    protected $encrypted = [
        'text_crypt', 
    ];


    protected $fillable = [
        'text', 'text_crypt', 'completed', "user_id"
    ];



    protected $dates = ['deleted_at'];
}
